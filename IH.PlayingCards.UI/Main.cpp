//Playing cards
//pt1 Ian Hawley
//pt2 Meredith Geoffrey



#include <iostream>
#include <conio.h>
using namespace std;

enum Suit { CLUBS, DIAMONDS, HEARTS, SPADES};
enum Rank{Two=2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};

struct Card{ 

		Rank Rank; 
		Suit Suit;
};
void PrintCard(Card card)
{
	
	switch(card.Rank)
	{
		case Two: 
			cout << "The Two";
			break;
		case Three: 
			cout << "The Three";
			break;
		case Four: 
			cout << "The Four";
			break;
		case Five: 
			cout << "The Five";
			break;
		case Six: 
			cout << "The Six";
			break;
		case Seven: 
			cout << "The Seven";
			break;
		case Eight: 
			cout << "The Eight";
			break;
		case Nine: 
			cout << "The Nine";
			break;
		case Ten: 
			cout << "The Ten";
			break;
		case Jack: 
			cout << "The Jack";
		case Queen: 
			cout << "The Queen";
			break;
		case King: 
			cout << "The King";
			break;
		case Ace: 
			cout << "The Ace";
			break;

	}
	switch(card.Suit)
	{
		case CLUBS: 
			cout << " of Clubs";
			break;
		case DIAMONDS: 
			cout << " of Diamonds";
			break;
		case HEARTS: 
			cout << " of Hearts";
			break;
		case SPADES: 
			cout << " of Spades";
			break;
	}

}
Card HighCard(Card card1, Card card2)
{
	Card HighCard;

	if (card1.Rank > card2.Rank)
	{
		HighCard = card1;
		PrintCard(card1);
	}
	else if (card1.Rank = card2.Rank)
	{
		if (card1.Suit > card2.Suit)
		{
			HighCard = card1;
			PrintCard(card1);
		}
		else
		{
			HighCard = card2;
			PrintCard(card2);
		}
	}
	else
	{
		HighCard = card2;
		PrintCard(card2);
	}
	return HighCard;
}

int main() {

	Card testCard1;
	testCard1.Rank = Queen;
	testCard1.Suit = HEARTS;

	Card testCard2;
	testCard2.Rank = Ace;
	testCard2.Suit = SPADES;

	HighCard(testCard1, testCard2);
	cout << "\n";
	PrintCard(testCard1);

	

	(void) _getch();
}